package com.weir.member.keycloak;

import java.util.Map;

import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

import org.eclipse.microprofile.jwt.JsonWebToken;
import org.jboss.resteasy.annotations.cache.NoCache;

import io.quarkus.oidc.UserInfo;
import io.quarkus.security.identity.SecurityIdentity;

@Path("users")
public class UsersResource {

	@Inject
    SecurityIdentity securityIdentity;
	
	@RolesAllowed("user")
	@GET
	@Path("me")
	public Map<String, String> me() {
		return Map.of("username", securityIdentity.getPrincipal().getName());
	}
	
	@Inject
	JsonWebToken jwt;
	
	@GET
	@Path("admin")
	@RolesAllowed("admin")
	public Map<String, String> name() {
		return Map.of("subject", jwt.getSubject(),
				"preferred_username", jwt.getClaim("preferred_username"));
	}
	
	@Inject
	UserInfo userInfo;
	
	@GET
	@Path("info")
	@RolesAllowed("user")
	@NoCache
	public Map<String, String> info() {
		return Map.of("sub", userInfo.getString("sub"),
				"email", userInfo.getString("email"));
	}
}
