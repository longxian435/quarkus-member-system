package com.weir.member.entity;

import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * 会员积分表
 * @author weir
 *
 */
@Entity
@Table(name = "member_account")
public class MemberAccount extends BaseEntity {
	
	
	private static final long serialVersionUID = 1503939391043537657L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	@Column(name = "member_id")
	public Long memberId;
	/**
	 * 金额
	 */
	public BigDecimal amount;
	/**
	 * 积分
	 */
	public BigDecimal score;

}
