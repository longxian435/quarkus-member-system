package com.weir.member.entity;

import jakarta.persistence.*;
import java.util.Date;

/**
 * 会员表
 * @author weir
 *
 */
@Entity
@Table(name = "member_info")
//@EntityListeners(JPAAuditorAware.class)
public class MemberInfo extends BaseEntity {
	
	private static final long serialVersionUID = -5620306830094420254L;

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	
	public String username;
	public String password;
	public String realname;
	public String phone;
	public String email;
	@Column(name = "head_image")
	public String headImage;

}
