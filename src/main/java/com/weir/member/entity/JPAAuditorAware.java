package com.weir.member.entity;

import java.net.InetAddress;
import java.net.UnknownHostException;

import jakarta.enterprise.context.RequestScoped;
import jakarta.persistence.PrePersist;

import com.weir.member.util.Sequence;

@RequestScoped
public class JPAAuditorAware {

	public JPAAuditorAware() {

	}

	@PrePersist
	public void prePersist(Object target) {
		MemberInfo m = (MemberInfo) target;
		try {
			Sequence sequence = new Sequence(1,11);
//			Sequence sequence = new Sequence(InetAddress.getLocalHost());
			m.id = sequence.nextId();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
