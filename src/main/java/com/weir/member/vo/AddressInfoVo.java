package com.weir.member.vo;

public class AddressInfoVo {

	public Integer id;
	public String address;
	public AddressInfoVo() {
		super();
	}
	public AddressInfoVo(Integer id, String address) {
		super();
		this.id = id;
		this.address = address;
	}
}
