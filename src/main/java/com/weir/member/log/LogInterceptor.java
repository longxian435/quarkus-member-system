package com.weir.member.log;

import lombok.extern.slf4j.Slf4j;

import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import java.lang.reflect.Method;

/**
 * @author yujianyou
 * @date 2021/11/29 14:41
 * @email 597907730@qq.com
 */
@Slf4j
@Log
@Interceptor
//@Priority(Interceptor.Priority.PLATFORM_BEFORE + 200)
public class LogInterceptor {

    @AroundInvoke
    public Object invocation(InvocationContext context) throws Exception {
        // 获取注解参数
        Method method = context.getMethod();
        String value = method.getAnnotation(Log.class).value();
        log.info("===============" + value);
        
        return context.proceed();
    }
}
